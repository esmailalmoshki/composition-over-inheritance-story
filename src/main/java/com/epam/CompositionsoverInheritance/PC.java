package com.epam.CompositionsoverInheritance;

class PC {
    private Moniter theMoniter;
    private Motherboard theMotherboard;
    private Case theCase;

    public PC(Moniter theMoniter, Motherboard themotherboard, Case theCase) {
        this.theMoniter = theMoniter;
        this.theMotherboard = themotherboard;
        this.theCase = theCase;
    }
}
class Moniter {
    private Resolution nativeResolution;
    private String model;
    private String manufacturer;
    private int  size;


    public Moniter(Resolution nativeResolution,String model, String manufacturer, int size) {
        this.nativeResolution=nativeResolution;
        this.model = model;
        this.manufacturer = manufacturer;
        this.size = size;
    }
}
class Resolution{
    private int width;
    private int height;

    public Resolution(int width, int height) {
        this.width = width;
        this.height = height;
    }
}

class Case {
    private Dimension dimension;
    private String model;
    private String manufacturer;
    private String powerSupply;

    public Case(Dimension dimension,String model, String manufacturer, String powerSupply) {
        this.model = model;
        this.manufacturer = manufacturer;
        this.powerSupply = powerSupply;
        this.dimension = dimension;
    }
}
class Dimension{
    private int width;
    private int height;
    private int depth;

    public Dimension(int width, int height, int depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }
}

class Motherboard {
    private String model;
    private String manufacturer;
    private String bios;
    private int ramslots;
    private int cardslots;

    public Motherboard(String model, String manufacturer,String bios, int ramslots, int cardslots) {
        this.model = model;
        this.manufacturer = manufacturer;
        this.bios = bios;
        this.ramslots = ramslots;
        this.cardslots = cardslots;
    }
}
