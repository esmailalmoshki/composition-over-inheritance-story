package com.epam.CompositionsoverInheritance;

class Person {
    private String title;
    private String name;
    int age;

    public Person(String title, String name, int age) {
        this.title = title;
        this.name = name;
        this.age = age;
    }

}

class Employee {
    private int salary;
    private Person person;

    public Employee(Person p, int salary) {
        this.person = p;
        this.salary = salary;
    }
}
