package com.epam.CompositionsoverInheritance;

public class useInheritance {
    public static void main(String[] args) {
        Object object=new Object();
        if (object.equals("is-a Relation")) {
            System.out.println("use inheritance");
        }
        if (object.equals("a part of Relation")) {
            System.out.println("use Composition");
        }
    }
}
